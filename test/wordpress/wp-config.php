<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'wordpress' );

/** Database password */
define( 'DB_PASSWORD', 'wordpress' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'AGL<Zu7auol?(^PbV.E:]&C]=93.Id+Zydrd}WVajBNm;=hB-!B7[pzW 7TDJ|#:' );
define( 'SECURE_AUTH_KEY',  'z]WffiQuoN )UK>^TzR!_W0ZW=mRp^t}t!=qhPHdHl#o!Z;`yGx&ue6, r*_ZmLD' );
define( 'LOGGED_IN_KEY',    '.VPZF.=V/!Brl(0SuCgg=JVG/hNB}%=L-tF:C8y?3KJ4s2^QA)v;SgJR]LV3n:II' );
define( 'NONCE_KEY',        '> ?7&#xX3$w_LFrE|zXBt%p)fBvjnvr]h|h46QHTS[_6|uF.5}56qe6i:*Coi9V2' );
define( 'AUTH_SALT',        'QCB6(8ExszUvFW1it4x+d}XX7zyj*4@+&X]V/;!i<I]}Q%5WH }S5ZqXp; .5Wi#' );
define( 'SECURE_AUTH_SALT', '?]if|gy`O_B5/1.X8}Dr_:inqG+yc^`O}=<b?T@%Pkj{Q2-Mc,W(0:+Y_HerqSb<' );
define( 'LOGGED_IN_SALT',   'H#8qGEkAZ5:Jjc3*=(_dqAMs.91&}9{T-Xss{oePLYy*4]/|b%l+rEqQR7=jf[hp' );
define( 'NONCE_SALT',       'H~Rd7l]0T2dZuqHe>2&Xu!9yh7}C1-Ch`bH^IUlS]F^8I=7%a[i!P|sondZ@~t1*' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
