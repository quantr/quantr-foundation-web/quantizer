<html>
	<script data-cfasync="false" type="text/javascript" src="jquery-3.6.0.js"></script>
	<script>
		$(document).on('ajaxError', function (event, xhr, options, exc) {
			alert('ajaxError');
			console.trace();
		});
		console.log('start');
		var xhr = new XMLHttpRequest(),
				method = "GET",
				url = "/api.php";

		xhr.addEventListener("progress", updateProgress);
		xhr.addEventListener("load", transferComplete);
		xhr.addEventListener("error", transferFailed);
		xhr.addEventListener("abort", transferCanceled);


		xhr.open(method, url, true);

		xhr.send();

//		xhr.abort();

		function updateProgress(oEvent) {
			if (oEvent.lengthComputable) {
				var percentComplete = oEvent.loaded / oEvent.total;
				console.log(percentComplete);
			} else {
				console.log('Unable to compute progress information since the total size is unknown');
			}
		}

		function transferComplete(evt) {
			console.log("transferComplete");
			console.log(evt);
		}

		function transferFailed(evt) {
			console.log("transferFailed");
			console.log(evt);
		}

		function transferCanceled(evt) {
			console.log("transferCanceled");
			console.log(evt);
		}
	</script>
	<body>
		abort example
	</body>
</html>