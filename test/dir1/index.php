<?php
	session_start();
?>
<!DOCTYPE html>
<!--
Copyright 2021 Peter <peter@quantr.hk>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<html>
	<head>
		<title>dir1</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<div>session_id=<?= session_id() ?></div>
		<div>key1=<?= $_SESSION['key1'] ?></div>
		<form method="post" action="save.php" enctype="MUltipart/form-data">
			<input type="file" name="myfile" />
			<input type="submit" value="upload" />
		</form>
		<form method="post" action="save.php">
			<input type="text" name="text1" id="text1" />
			<input type="submit" value="upload" />
		</form>
		<?php var_dump(highlight_string("<?\n". var_export($_SERVER, true))); ?>
		<?php
			$_SESSION['key1']=date('Y/m/d h:n:s');
		?>
	</body>
</html>
