/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.quantizer;

import hk.quantr.quantizer.fastcgi.FastCGI;
import hk.quantr.quantizer.fastcgi.FastCGIReturn;
import hk.quantr.quantizer.http.HttpEntity;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class HttpListener implements Runnable {

	private static final Logger logger = Logger.getLogger(HttpListener.class.getName());

//	final File WEB_ROOT = new File(Server.class.getResource("/webRoot").getPath());
	final String DEFAULT_FILES[] = {"index.html", "index.htm", "index.php"};
//		final String FILE_NOT_FOUND = "404.html";
	final String METHOD_NOT_SUPPORTED = "not_supported.html";
	private Socket socket = null;
	File webRoot;

	public HttpListener(Socket socket, File webRoot) {
		this.socket = socket;
		this.webRoot = webRoot;
	}

	@Override
	public void run() {
		BufferedReader in = null;
		PrintWriter out = null;
		BufferedOutputStream dataOut = null;

		try {
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream());
			dataOut = new BufferedOutputStream(socket.getOutputStream());

			String input;
//			do {
			String httpRequestStr = "";
			do {
				input = in.readLine();
				if (input != null) {
					httpRequestStr += input + "\n";
				}
			} while (input != null && !input.trim().equals(""));

			if (httpRequestStr.length() != 0) {
				HttpEntity httpEntity = parseHttp(httpRequestStr);
				System.out.println(httpEntity.content);
//				logger.log(Level.INFO, "path = " + httpEntity.path);

				logger.log(Level.INFO, httpEntity.method + " : " + httpEntity.path);

				if (httpEntity.method.equals("GET") || httpEntity.method.equals("HEAD")) {
					File file = null;

					if (httpEntity.file.endsWith("/")) {
						for (String defaultFile : DEFAULT_FILES) {
							file = new File(webRoot, defaultFile);
							if (file.exists()) {
								break;
							}
						}
					} else {
						file = new File(webRoot, httpEntity.file);
					}

					if (file.exists()) {
						if (file.getName().endsWith(".php")) {
							logger.log(Level.INFO, "GET FastCGI {0}", file.getAbsolutePath());
							FastCGIReturn r = FastCGI.get("127.0.0.1", 1992, file.getAbsolutePath(), httpEntity);
							byte[] fileData = r.content.getBytes();
							String fileDataStr = new String(fileData);
							if (fileDataStr.contains("Status:")) {
								List<String> lines = List.of(fileDataStr.split("\n")).stream().filter(name -> name.startsWith("Status:")).toList();
								String statusLine = lines.get(0);
								out.println("HTTP/1.1 " + statusLine.replace("Status: ", ""));
							} else {
								out.println("HTTP/1.1 200 OK");
							}
							out.println("Server: Quantizer : 1.0");
							out.println("Date: " + new Date());
							out.flush();
							System.out.println("1. " + fileData.length);
							System.out.println("2. " + r.bytes.length);
//							dataOut.write(fileData, 0, fileData.length);
							dataOut.write(r.bytes);
							dataOut.flush();
						} else {
							logger.log(Level.INFO, file.getAbsolutePath());
							byte[] fileData = FSUtil.readFileToByteArray(file);
							String contentType = getContentType(file.getName());
							out.println("HTTP/1.1 200 OK");
							out.println("Server: Quantizer : 1.0");
							out.println("Date: " + new Date());
							out.println("Content-type: " + contentType);
							out.println("Content-length: " + fileData.length);
							out.println();
							out.flush();
							dataOut.write(fileData, 0, fileData.length);
							dataOut.flush();
						}
					} else {
						out.println("HTTP/1.1 404 Not Found");
						out.println("Server: Quantizer : 1.0");
						out.println("Date: " + new Date());
						out.flush();
					}
				} else if (httpEntity.method.equals("POST")) {
//					if (httpEntity.data.get("Content-Type").indexOf("multipart/form-data") != -1) {
//						byte[] bytes = IOUtils.toByteArray(in);
//						System.out.println(new String(bytes));
//					}
					int contentLength = Integer.parseInt(httpEntity.data.get("Content-Length"));
					char[] buffer = new char[contentLength];
					in.read(buffer, 0, contentLength);
					String postBody = new String(buffer);

					File file = new File(webRoot, httpEntity.file);
					byte[] fileData;

					if (file.getName().endsWith(".php")) {
						logger.log(Level.INFO, "POST FastCGI {0}", file.getAbsolutePath());
						FastCGIReturn r = FastCGI.post("127.0.0.1", 1992, file.getAbsolutePath(), httpEntity, postBody);
//						fileData = r.content.getBytes();
						fileData = r.bytes;
						String fileDataStr = new String(fileData);
						if (fileDataStr.contains("Status:")) {
							List<String> lines = List.of(fileDataStr.split("\n")).stream().filter(name -> name.startsWith("Status:")).toList();
							String statusLine = lines.get(0);
							out.println("HTTP/1.1 " + statusLine.replace("Status: ", ""));
							out.println("Server: Quantizer : 1.0");
							out.println("Date: " + new Date());
						} else {
							out.println("HTTP/1.1 200 OK");
							out.println("Server: Quantizer : 1.0");
//							out.println("Content-Type: " + httpEntity.data.get("Accept"));
							out.println("Date: " + new Date());
						}
//						System.out.println(r.httpHeader);
//						String location = r.httpHeaders.get("Location");
//						if (location != null) {
//							HttpEntity httpEntity2 = new HttpEntity();
//							httpEntity2.path = location;
//							httpEntity2.method = "GET";
//							FastCGIReturn r2 = FastCGI.get("127.0.0.1", 1992, file.getAbsolutePath(), httpEntity2);
//							fileData = r2.content.getBytes();
//						}
					} else {
						logger.log(Level.INFO, file.getAbsolutePath());
						fileData = FSUtil.readFileToByteArray(file);
						String contentType = getContentType(file.getName());
						out.println("HTTP/1.1 200 OK");
						out.println("Server: Quantizer : 1.0");
						out.println("Date: " + new Date());
						out.println("Content-type: " + contentType);
						out.println("Content-length: " + fileData.length);
						out.println();
						out.flush();
					}
					out.flush();

					dataOut.write(fileData, 0, fileData.length);
					dataOut.flush();
				} else {
					logger.log(Level.WARNING, "501 Not Implemented : {0}", httpEntity.method);

					File file = new File(webRoot, METHOD_NOT_SUPPORTED);
					int fileLength = (int) file.length();
					String contentMimeType = "text/html";
					byte[] fileData = FSUtil.readFileToByteArray(file);

					out.println("HTTP/1.1 501 Not Implemented");
					out.println("Server: Quantizer : 1.0");
					out.println("Date: " + new Date());
					out.println("Content-type: " + contentMimeType);
					out.println("Content-length: " + fileLength);
					out.println();
					out.flush();

					dataOut.write(fileData, 0, fileLength);
					dataOut.flush();
				}
			}
//			} while (input != null);
		} catch (Exception ex) {
			logger.log(Level.SEVERE, "An exception was thrown", ex);
			out.println("HTTP/1.1 500 OK");
			out.println("Server: Quantizer : 1.0");
			out.println();
			out.println(ex.getMessage());
			out.flush();
		} finally {
			try {
				in.close();
				out.close();
				dataOut.close();
				socket.close();
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Error closing stream", e);
			}
		}
	}

	private String getContentType(String filename) {
		if (filename.endsWith(".htm") || filename.endsWith(".html") || filename.endsWith(".php")) {
			return "text/html";
		} else if (filename.endsWith(".js")) {
			return "text/javascript";
		} else if (filename.endsWith(".css")) {
			return "text/css";
		} else {
			return "text/plain";
		}
	}

	private HttpEntity parseHttp(String str) {
		if (str.length() > 102400) {
			return null;
		}
		HttpEntity httpEntity = new HttpEntity();
		httpEntity.content = str;
		String firstLine = str.split("\n")[0];
		httpEntity.method = firstLine.split(" ")[0];
		httpEntity.file = firstLine.split(" ")[1].replaceAll("\\?.*", "");
		httpEntity.path = firstLine.split(" ")[1];
		httpEntity.protocol = firstLine.split(" ")[2];
		httpEntity.data = new HashMap<>();
		for (String line : str.split("\n")) {
			if (!line.contains(":")) {
				continue;
			}
			line = line.trim();
//			System.out.println("\t" + line);
			int x = line.indexOf(":");
			httpEntity.data.put(line.substring(0, x).trim(), line.substring(x + 1).trim());
		}
		return httpEntity;
	}
}
