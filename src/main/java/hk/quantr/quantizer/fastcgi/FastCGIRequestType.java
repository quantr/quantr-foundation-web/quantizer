/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.quantizer.fastcgi;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class FastCGIRequestType {

	public static byte FCGI_BEGIN_REQUEST = 1;
	public static byte FCGI_ABORT_REQUEST = 2;
	public static byte FCGI_END_REQUEST = 3;
	public static byte FCGI_PARAMS = 4;
	public static byte FCGI_STDIN = 5;
	public static byte FCGI_STDOUT = 6;
	public static byte FCGI_STDERR = 7;
	public static byte FCGI_DATA = 8;
	public static byte FCGI_GET_VALUES = 9;
	public static byte FCGI_GET_VALUES_RESULT = 10;
}
