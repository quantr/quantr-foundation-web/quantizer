/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.quantizer.fastcgi;

import hk.quantr.javalib.CommonLib;
import hk.quantr.quantizer.http.HttpEntity;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class FastCGI {

	private static int request_id = 1;
	private static final Logger logger = Logger.getLogger(FastCGI.class.getName());

	public static FastCGIReturn get(String host, int port, String filepath, HttpEntity httpEntity) throws IOException {
		Socket client = new Socket(host, port);

		InputStream in = client.getInputStream();
		OutputStream out = client.getOutputStream();

		byte[] begin_request_body = new byte[8];
		begin_request_body[0] = 0;
		begin_request_body[1] = FastCGIRole.FCGI_RESPONDER;
		begin_request_body[2] = 0;
		byte[] begin_request = getPacket(FastCGIRequestType.FCGI_BEGIN_REQUEST, request_id, begin_request_body);
		out.write(begin_request);

		Map<String, String> params = new LinkedHashMap<>();
		params.put("GATEWAY_INTERFACE", "FastCGI/1.0");
		params.put("REQUEST_METHOD", httpEntity.method);
		params.put("REQUEST_URI", httpEntity.path);
		if (httpEntity.path.length() >= httpEntity.file.length() + 1) {
			params.put("QUERY_STRING", httpEntity.path.substring(httpEntity.file.length() + 1));
		}
		params.put("SCRIPT_FILENAME", filepath);
		params.put("SERVER_NAME", "Quantizer");
		params.put("SERVER_PROTOCOL", "HTTP/1.1");
		if (httpEntity.data != null) {
			for (String key : httpEntity.data.keySet()) {
				params.put("HTTP_" + key.toUpperCase(), httpEntity.data.get(key));
			}
		}

		ByteBuffer paramContainer = ByteBuffer.allocate(10240000);
		for (String key : params.keySet()) {
			logger.info("  > " + key + " = " + params.get(key));
			byte[] onePair = getParam(key, params.get(key));
			paramContainer.put(onePair);
		}
		paramContainer.flip();
		byte[] fcgi_param_byte = new byte[paramContainer.remaining()];
		paramContainer.get(fcgi_param_byte);
		byte[] fcgi_params = getPacket(FastCGIRequestType.FCGI_PARAMS, request_id, fcgi_param_byte);
		out.write(fcgi_params);
		byte[] fcgi_params_end = getPacket(FastCGIRequestType.FCGI_PARAMS, request_id, new byte[0]);
		out.write(fcgi_params_end);
		byte[] fcgi_stdin = getPacket(FastCGIRequestType.FCGI_STDIN, request_id, new byte[0]);
		out.write(fcgi_stdin);
		out.flush();

		byte[] bytes = IOUtils.toByteArray(in);

		int endOffset;
		int currentPointer = 0;
		int contentLength;
		FastCGIReturn r = new FastCGIReturn();
		do {
			int offset = currentPointer;
			byte fcgi_content_header[] = Arrays.copyOfRange(bytes, offset, offset + 8);
			int padding = fcgi_content_header[6];
			int type = fcgi_content_header[1];
			if (type == FastCGIRequestType.FCGI_END_REQUEST) {
				break;
			} else if (type == FastCGIRequestType.FCGI_STDOUT || type == FastCGIRequestType.FCGI_STDERR) {
				contentLength = (int) CommonLib.getShort(fcgi_content_header[5], fcgi_content_header[4]);
				endOffset = offset + 8 + contentLength;

				byte temp[] = Arrays.copyOfRange(bytes, offset + 8, endOffset);
				r.bytes = CommonLib.appendByteArray(r.bytes, temp);
				r.content += new String(temp);
				currentPointer = endOffset + padding;
			} else {
				System.out.println("Unhandle type");
				System.exit(2);
				break;
			}
		} while (endOffset < bytes.length);
		client.close();

		request_id++;
		return r;
	}

	public static FastCGIReturn post(String host, int port, String filepath, HttpEntity httpEntity, String postBody) throws IOException {
		Socket client = new Socket(host, port);

		InputStream in = client.getInputStream();
		OutputStream out = client.getOutputStream();

		byte[] begin_request_body = new byte[8];
		begin_request_body[0] = 0;
		begin_request_body[1] = FastCGIRole.FCGI_RESPONDER;
		begin_request_body[2] = 0;
		byte[] begin_request = getPacket(FastCGIRequestType.FCGI_BEGIN_REQUEST, request_id, begin_request_body);
		out.write(begin_request);

		HashMap<String, String> params = new HashMap<>();
		params.put("GATEWAY_INTERFACE", "FastCGI/1.0");
		params.put("REQUEST_METHOD", httpEntity.method);
		params.put("REQUEST_URI", httpEntity.path);
		if (httpEntity.path.length() >= httpEntity.file.length() + 1) {
			params.put("QUERY_STRING", httpEntity.path.substring(httpEntity.file.length() + 1));
		}
		params.put("SCRIPT_FILENAME", filepath);
		params.put("SERVER_NAME", "Quantizer");
		params.put("SERVER_PROTOCOL", "HTTP/1.1");
		params.put("CONTENT_TYPE", httpEntity.data.get("Content-Type"));//"application/x-www-form-urlencoded");
		params.put("CONTENT_LENGTH", String.valueOf(postBody.length()));
		for (String key : httpEntity.data.keySet()) {
			params.put("HTTP_" + key.toUpperCase(), httpEntity.data.get(key));
			params.put(key, httpEntity.data.get(key));
		}
		params.put("SCRIPT_NAME", "/index.php");

		ByteArrayOutputStream paramBytes = new ByteArrayOutputStream();

//		ByteBuffer paramContainer = ByteBuffer.allocate(102400);
		for (String key : params.keySet()) {
			byte[] onePair = getParam(key, params.get(key));
			paramBytes.write(onePair);
//			paramContainer.put(onePair);
		}
//		paramContainer.flip();
//		byte[] fcgi_param_byte = new byte[paramContainer.remaining()];
//		paramContainer.get(fcgi_param_byte);
		byte[] fcgi_params = getPacket(FastCGIRequestType.FCGI_PARAMS, request_id, paramBytes.toByteArray());
		out.write(fcgi_params);

		byte[] fcgi_params_end = getPacket(FastCGIRequestType.FCGI_PARAMS, request_id, new byte[0]);
		out.write(fcgi_params_end);

		byte[] fcgi_stdin = getPacket(FastCGIRequestType.FCGI_STDIN, request_id, postBody.getBytes());
		out.write(fcgi_stdin);

		byte[] fcgi_stdin_end = getPacket(FastCGIRequestType.FCGI_STDIN, request_id, new byte[0]);
		out.write(fcgi_stdin_end);
		out.flush();

		byte[] bytes = IOUtils.toByteArray(in);

		int endOffset;
		int currentPointer = 0;
		int contentLength;
		String s = "";
		FastCGIReturn r = new FastCGIReturn();
		do {
			int offset = currentPointer;
			byte fcgi_content_header[] = Arrays.copyOfRange(bytes, offset, offset + 8);
			int padding = fcgi_content_header[6];
			int type = fcgi_content_header[1];
			if (type == FastCGIRequestType.FCGI_END_REQUEST) {
				break;
			} else if (type == FastCGIRequestType.FCGI_STDOUT || type == FastCGIRequestType.FCGI_STDERR) {
				contentLength = (int) CommonLib.getShort(fcgi_content_header[5], fcgi_content_header[4]);
				endOffset = offset + 8 + contentLength;
//				s += new String(Arrays.copyOfRange(bytes, offset + 8, endOffset));
				byte temp[] = Arrays.copyOfRange(bytes, offset + 8, endOffset);
				r.bytes = CommonLib.appendByteArray(r.bytes, temp);
				r.content += new String(temp);

				currentPointer = endOffset + padding;
			} else {
				System.out.println("Unhandle type");
				break;
			}
		} while (endOffset < bytes.length);
		client.close();

		r.parseHttpHeader(r.content);
		request_id++;
		return r;
	}

	public static byte[] getPacket(byte type, int id, byte[] content) {
		byte version = 1;
		byte[] header = new byte[8];
		header[0] = version;
		header[1] = type;
		header[2] = (byte) ((id >> 8) & 0xff);
		header[3] = (byte) (id & 0xff);
		header[4] = (byte) ((content.length >> 8) & 0xff);
		header[5] = (byte) (content.length & 0xff);
		header[6] = 0;
		header[7] = 0;

		byte[] packet = new byte[header.length + content.length];
		System.arraycopy(header, 0, packet, 0, header.length);
		System.arraycopy(content, 0, packet, header.length, content.length);

		return packet;
	}

	public static byte[] getParam(String name, String value) {
		int nameLength = name.length();
		if (value == null) {
			return new byte[0];
		}
		int valueLength = value.length();
		byte[] nameLen, valueLen;
		if (nameLength < 128) {
			nameLen = new byte[1];
			nameLen[0] = (byte) nameLength;
		} else {
			nameLen = CommonLib.getByteArray(nameLength);
			ArrayUtils.reverse(nameLen);
			nameLen[0] = (byte) (nameLen[0] | 0x80);
		}
		if (valueLength < 128) {
			valueLen = new byte[1];
			valueLen[0] = (byte) valueLength;
		} else {
			valueLen = CommonLib.getByteArray(valueLength);
			ArrayUtils.reverse(valueLen);
			valueLen[0] = (byte) (valueLen[0] | 0x80);
		}
		byte[] onePair = new byte[nameLen.length + valueLen.length + name.getBytes().length + value.getBytes().length];
		System.arraycopy(nameLen, 0, onePair, 0, nameLen.length);
		System.arraycopy(valueLen, 0, onePair, nameLen.length, valueLen.length);
		System.arraycopy(name.getBytes(), 0, onePair, nameLen.length + valueLen.length, name.getBytes().length);
		System.arraycopy(value.getBytes(), 0, onePair, nameLen.length + valueLen.length + name.getBytes().length, value.getBytes().length);
		return onePair;
	}
}
