/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.quantizer.fastcgi;

import java.util.HashMap;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class FastCGIReturn {

	public byte[] bytes = {};
	public String content = "";
	public String httpHeader;
	public HashMap<String, String> httpHeaders = new HashMap<>();

	public void parseHttpHeader(String str) {
		httpHeader = str;
		for (String line : str.split("\r\n")) {
			if (line.length() == 0) {
				break;
			}
			if (line.contains(":")) {
				httpHeaders.put(line.split(":")[0], line.split(":")[1]);
			}
		}

	}
}
