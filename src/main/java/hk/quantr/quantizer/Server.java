/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.quantizer;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Server {

	private int port;
	File webRoot;

	private static final Logger logger = Logger.getLogger(Server.class.getName());

	public Server(int port, File webRoot) {
		this.port = port;
		this.webRoot = webRoot;
	}

	public void start() {
		try {
			ServerSocket serverConnect = new ServerSocket(port);
			serverConnect.setReuseAddress(true);
			while (true) {
				HttpListener myServer = new HttpListener(serverConnect.accept(), webRoot);
				Thread thread = new Thread(myServer);
				thread.start();
			}
		} catch (IOException ex) {
			logger.log(Level.SEVERE, null, ex);
		}
	}

}
