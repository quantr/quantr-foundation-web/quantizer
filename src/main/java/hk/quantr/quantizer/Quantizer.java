package hk.quantr.quantizer;

import hk.quantr.hkdataformat.HKDataFormat;
import hk.quantr.hkdataformat.HKDataFormatLower;
import hk.quantr.hkdataformat.datastructure.Node;
import hk.quantr.javalib.PropertyUtil;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Quantizer {

	private static final Logger logger = Logger.getLogger(Quantizer.class.getName());

	static {
		InputStream stream = Quantizer.class.getClassLoader().getResourceAsStream("logging.properties");
		try {
			LogManager.getLogManager().readConfiguration(stream);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void main(String args[]) throws ParseException, IOException {
		logger.log(Level.INFO, "start");
//		System.out.println(PropertyUtil.getProperty("main.properties", "build.date"));

		Options options = new Options();
		options.addOption("v", "version", false, "display version");
		options.addOption("h", "help", false, "help");
		options.addOption(Option.builder("c")
				.required(true)
				.hasArg()
				.argName("file")
				.desc("config file")
				.longOpt("config")
				.build());

		if (args.length == 0 || Arrays.asList(args).contains("-h") || Arrays.asList(args).contains("--help")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("java -jar quantizer-xx.jar [OPTION] <input file>", options);
			return;
		}
		if (Arrays.asList(args).contains("-v") || Arrays.asList(args).contains("--version")) {
			System.out.println("version : " + PropertyUtil.getProperty("main.properties", "version"));

			TimeZone utc = TimeZone.getTimeZone("UTC");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			format.setTimeZone(utc);
			Calendar cl = Calendar.getInstance();
			try {
				Date convertedDate = format.parse(PropertyUtil.getProperty("main.properties", "build.date"));
				cl.setTime(convertedDate);
				cl.add(Calendar.HOUR, 8);
			} catch (java.text.ParseException ex) {
				Logger.getLogger(Quantizer.class.getName()).log(Level.SEVERE, null, ex);
			}
			System.out.println("build date : " + format.format(cl.getTime()) + " HKT");
			return;
		}

		CommandLineParser cliParser = new DefaultParser();
		CommandLine cmd = cliParser.parse(options, args);
		String configFile = cmd.getOptionValue("c");

		initFastCGI(new File(configFile));
		loadHost(new File(configFile));
	}

	private static void initFastCGI(File configFile) throws IOException {
		String command = HKDataFormat.get(configFile, "/quantizer/init/phpcgi/command");
		String dir = HKDataFormat.get(configFile, "/quantizer/init/phpcgi/dir").replaceFirst("^~", System.getProperty("user.home"));
		String execCommand = dir + File.separator + command;
		logger.log(Level.SEVERE, "initFastCGI {0}", execCommand);
		Process p = Runtime.getRuntime().exec(execCommand);
	}

	public static void loadHost(File configFile) throws IOException {
		logger.log(Level.SEVERE, "reading ", configFile);
		ArrayList<Node> nodes = HKDataFormatLower.getNodes(configFile, "/quantizer/hosts/host");
		for (Node host : nodes) {
			int port = (int) host.get("port").value;
			File webRoot = new File((String) host.get("path").value);
			if (webRoot.isDirectory()) {
				Server server = new Server(port, webRoot);
				new Thread(() -> {
					server.start();
				}).start();
				logger.log(Level.SEVERE, "init host {0}", port + ", " + webRoot.getAbsolutePath());
			} else {
				logger.log(Level.SEVERE, "init host failed {0}", port + ", " + webRoot.getAbsolutePath());
			}
		}
	}
}
